<?php

namespace App\Repository;

interface ProductRepositoryInterface extends BaseRepositoryInterface{

    public function productImage($pram,$product_id);

    public function singleFilter($columnName);

    public function productFilter($columnName,$operator,$value);

//    public function createdProduct($value);
}
