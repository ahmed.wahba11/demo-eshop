<?php

namespace App\Repository;

use Illuminate\Http\Request;

interface BaseRepositoryInterface {
    public function all($relations);

    public function createNew($request);

    public function update($column,$id,$request);

    public function showSingleRow($column,$id);

    public function delete($column,$id);

}
