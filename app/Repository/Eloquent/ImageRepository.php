<?php

namespace App\Repository\Eloquent;

use App\Models\Image;
use App\Repository\ImageRepositoryInterface;

class ImageRepository extends BaseRepository implements ImageRepositoryInterface{

    protected $model;

    public function __construct(Image $model)
    {
        $this->model = $model;
    }
}
