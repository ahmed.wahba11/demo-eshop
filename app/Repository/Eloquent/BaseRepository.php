<?php

namespace App\Repository\Eloquent;


use App\Repository\BaseRepositoryInterface;
use Illuminate\Http\Request;

class BaseRepository implements BaseRepositoryInterface{
    public function all($relations=[])
    {
        if(count($relations) == 0) {
            return $this->model::query()->get();
        }
        return $this->model::query()->with($relations)->get();
    }

    public function createNew($request)
    {
        return $this->model::query()->create($request);
    }
    public function update($column,$id,$request)
    {
        return $this->model::query()->where($column,'=',$id)->update($request);
    }

    public function showSingleRow($column, $id)
    {
        return $this->model->query()->where($column,'=',$id)->get();
    }

    public function delete($column,$id)
    {
        return $this->model::query()->where($column,'=',$id)->delete();
    }
}
