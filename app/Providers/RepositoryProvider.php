<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Repository\ProductRepositoryInterface',
            'App\Repository\Eloquent\ProductRepository'
        );
//        $this->app->bind(
//            'App\Repository\UserRepositoryInterface',
//            'App\Repository\Eloquent\UserRepository'
//        );
        $this->app->bind(
            'App\Repository\ImageRepositoryInterface',
            'App\Repository\Eloquent\ImageRepository'
        );

        $this->app->bind(
            'App\Repository\UserRepositoryInterface',
            'App\Repository\Eloquent\UserRepository'
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
