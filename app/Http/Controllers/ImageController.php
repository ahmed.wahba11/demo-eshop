<?php

namespace App\Http\Controllers;

use App\Models\Image;
use App\Models\Product;
use App\Repository\ImageRepositoryInterface;
use App\Repository\ProductRepositoryInterface;
use Illuminate\Http\Request;

class ImageController extends Controller
{
    private $image;
    private $products;
    protected $colors = ['Pink', 'Crimson', 'Red', 'Maroon', 'Brown', 'Misty', 'Rose', 'Salmon',
        'Coral', 'Orange-Red', 'Chocolate', 'Orange', 'Gold', 'Ivory', 'Yellow', 'Olive',
        'Yellow-Green', 'Lawn green', 'Chartreuse', 'Lime', 'Green Spring', 'green',
        'Aquamarine', 'Turquoise', 'Azure', 'Aqua/Cyan', 'Teal', 'Lavender', 'Blue', 'Navy',
        'Blue-Violet', 'Indigo', 'Dark Violet', 'Plum', 'Magenta', 'Purple', 'Red-Violet',
        'Tan', 'Beige', 'Slate gray', 'Dark Slate Gray', 'White', 'White Smoke', 'Light Gray'
        , 'Silver', 'Dark Gray', 'Gray', 'Dim Gray', 'Black'];


    public function __construct(ImageRepositoryInterface $image, ProductRepositoryInterface $products)
    {
        $this->image = $image;
        $this->products = $products;

    }

    public function addImages($id)
    {
        return view('add-images', [
            'categories' => $this->products->singleFilter('category'),
            'brands' => $this->products->singleFilter('brand'),
            'colors' => $this->colors,
        ]);
    }

    public function addNewImages(Request $request, $id)
    {

        $imageName = time() . rand(1, 100) . '-' . $id . '.'
            . $request->file(['image'])->getClientOriginalExtension();

        $request->file(['image'])->move(public_path('/images/products/'), $imageName);

        $this->image->createNew(
            $request->all('color') + ['main' => 0] + ['image' => $imageName]
            + ['product' => $id]);


        return redirect('product-details/' . $id);
    }

//    public function imageDelete($id)
//    {
//        $image = Image::query()->find($id);
//        return redirect()->back();
//
//    }
    public function deleteImage($id)
    {
        $data = Image::query()->find($id);
        $data->delete();

        return redirect('/');
    }


}
