<?php

namespace App\Http\Controllers;


use App\Models\Product;
use App\Models\User;
use App\Repository\ImageRepositoryInterface;
use App\Repository\ProductRepositoryInterface;
use Illuminate\Http\Request;

class ProductController extends Controller
{

    private $products;
    private $images;
    protected $colors =['Pink','Crimson','Red','Maroon','Brown','Misty','Rose','Salmon',
        'Coral','Orange-Red','Chocolate','Orange','Gold','Ivory', 'Yellow','Olive',
        'Yellow-Green','Lawn green','Chartreuse','Lime','Green Spring','green',
        'Aquamarine','Turquoise','Azure','Aqua/Cyan', 'Teal','Lavender','Blue','Navy',
        'Blue-Violet','Indigo','Dark Violet','Plum','Magenta','Purple','Red-Violet',
        'Tan','Beige','Slate gray','Dark Slate Gray', 'White','White Smoke','Light Gray'
        ,'Silver', 'Dark Gray','Gray','Dim Gray','Black'];

    public function __construct(ProductRepositoryInterface $products,
                                ImageRepositoryInterface $images)
    {
        $this->products = $products;
        $this->images = $images;
    }

    /**
     * Display a everything in home page like products , list of categories and brands
     *
     * @return
     */
    public function index()
    {

        return view('home', [
            'products' => $this->products->productImage(1),
            'categories' => $this->products->singleFilter('category'),
            'brands' => $this->products->singleFilter('brand'),
        ]);
    }

    /**
     * Display all products and filter them by category
     * @param $value .. the name of category
     * @return
     */
    public function filterByCategory($value)
    {

        return view('products', [

            'categories' => $this->products->singleFilter('category'),
            'brands' => $this->products->singleFilter('brand'),
            'productsFilter' => $this->products->productFilter('category', '=', $value)

        ]);
    }

    /**
     * Display all products and filter them by brand
     * @param $value .. name of the brand
     * @return
     */
    public function filterByBrand($value)
    {
        return view('products', [

            'brands' => $this->products->singleFilter('brand'),
            'categories' => $this->products->singleFilter('category'),
            'productsFilter' => $this->products->productFilter('brand', '=', $value)

        ]);
    }

    /**
     * Show the Product Details
     * @param $id .. the id of the product
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function productDetails($id)
    {

        return view('product-details', [
            'product' => $this->products->productImage('', $id),
            'brands' => $this->products->singleFilter('brand'),
            'categories' => $this->products->singleFilter('category'),
        ]);
    }

    /**
     * Show the Form to Add ne product
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function NewProduct()
    {
        $users = User::query()->get(['id', 'name']);
        return view('new-product', [
            'brands' => $this->products->singleFilter('brand'),
            'categories' => $this->products->singleFilter('category'),
            'users' => $users,
            'colors'=>$this->colors,
        ]);
    }

    /**
     * Create new Product and the main Image of this Product
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function createNewProduct(Request $request)
    {
        if($this->products->createNew($request->except('image','color','main'))){
            $imageName = time().rand(1,100). '-' .
                Product::query()->latest()->get('id')->first()->id .'.'
                .$request->file(['image'])->getClientOriginalExtension();


            $request->file(['image'])->move(public_path('/images/products/'), $imageName);

            $this->images->createNew(
                $request->only('color') + ['main'=>1] +['image'=>$imageName]
                +['product'=>Product::query()->latest()->get('id')->first()->id]);
        }


        return redirect('product-details/'.Product::query()->latest()->get('id')->first()->id);
    }

    public function updatePage($id)
    {

        return view('update-product',[
            'brands' => $this->products->singleFilter('brand'),
            'categories' => $this->products->singleFilter('category'),
            'product' => $this->products->showSingleRow('id',$id),
            'images'=>$this->images->showSingleRow('product',$id),

        ]);
    }

    public function updateProduct(Request $request,$id)
    {
        $this->products->update('id',$id,$request->except('_token'));

        return redirect('product-details/'.$id);
    }

    public function removeProduct($id)
    {
        Product::query()->where('id','=',$id)->delete();

        return redirect('/');

    }

}
