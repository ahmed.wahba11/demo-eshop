<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'ProductController@index')->name('home');
Route::get('/products/{category}', 'ProductController@filterByCategory')->name('products-category');
Route::get('/brand/{brand}', 'ProductController@filterByBrand')->name('products-brand');
Route::get('/product-details/{id}', 'ProductController@productDetails')->name('product-details');

Route::get('new-product/','ProductController@NewProduct');
Route::post('/','ProductController@createNewProduct')->name('new-product');

Route::get('update-product/{id}','ProductController@updatePage');
Route::post('update-product/{id}','ProductController@updateProduct')->name('update-product');

Route::get('add-images/{id}','ImageController@addImages');
Route::Post('add-images/{id}','ImageController@addNewImages')->name('add-images');


//Route::get('delete/{id}','ImageController@imageDelete');
Route::delete('delete/{id}','ImageController@deleteImage')->name('delete-image');

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');
