@extends('base')
@section('contant')
<div class="container">
    <div class="row">
       <div class="product-images col-7">
        <h6><a href="{{route('home')}}">Home</a> /
            <a href="{{url('products/'.$product[0]->category)}}">{{$product[0]->category}}</a> /
            <a href="{{url('brand/'.$product[0]->brand)}}">{{$product[0]->brand}}</a></h6>
           <div>
               @for($i=0;$i<count($product);$i++)
                   @if($product[$i]->main === 1)
                        <div class="main-img">
                            <img class ="img-thumbnail"
                                 style="width: 500px;height: 500px"
                                src="{{asset('images/products/'.$product[$i]->image)}}"
                                  alt="...">
                        </div>
                   @endif
               @endfor
                       @for($i=0;$i<count($product);$i++)
                   @if($product[$i]->main === 0)
                        <div style="display: inline;width: 500px;height: 500px">
                            <img class="img-thumbnail small-img"
                                 style="width: 100px;height: 100px;margin-top: 20px"
                                 src="{{asset('images/products/'.$product[$i]->image)}}"
                                  alt="...">
                        </div>
                   @endif
               @endfor
           </div>
       </div>
        <div class="product-title col-5">
            <h3>{{$product[0]->title}} for {{$product[0]->category}}</h3>
            @if($product[0]->discount == 0)
                <p class="main-price">${{$product[0]->price}}</p>
            @else
                <p> <strong class="old-price">${{$product[0]->price}}</strong>
                    <strong class="discount-price">
                        ${{$product[0]->price - (($product[0]->discount*$product[0]->price)/100)}}</strong>
                </p>
            @endif

            <div class="description">
                <p>{{$product[0]->description}}</p>
            </div>

        </div>
    </div>

    <div class="add-product pt-5">
        <a href="{{url('new-product/')}}" class="btn btn-primary" >Add New Product</a>
        <a href="{{url('add-images/'.$product[0]->id)}}" class="btn btn-primary" >Add images</a>
        <a href="{{url('update-product/'.$product[0]->id)}}" class="btn btn-primary" >Update Product</a>
    </div>

</div>


@endsection
