@extends('base')
@section('contant')

{{--{{dd($images)}}--}}
    <div class="new-product">
        <h2>Update Product</h2>
        <form  action="{{url('update-product/'.Request::Route('id'))}}" method="post" enctype="multipart/form-data">
            @csrf

            <div class="mb-3 row">
                <label for="title" class="col-sm-2 col-form-label">title</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="title"
                           name="title" placeholder="Title of the product"
                    value="{{$product[0]->title}}">
                </div>
            </div>
            <div class="mb-3 row">
                <label for="description" class="col-sm-2 col-form-label">Description </label>
                <div class="col-sm-10">
                <textarea class="form-control" id="description" rows="3" name="description"
                          placeholder="Product Description" >{{$product[0]->description}}</textarea>
                </div>
            </div>
            <div class="mb-3 row">
                <label for="price" class="col-sm-2 col-form-label" >price</label>
                <div class="col-sm-4">
                    <input type="number" class="form-control" id="price" name="price"
                           placeholder="Price" value="{{$product[0]->price}}">
                </div>

                <label for="discount" class="col-sm-2 col-form-label">discount</label>
                <div class="col-sm-4">
                    <input type="number" class="form-control" id="discount"
                           name="discount" placeholder="discount" value="{{$product[0]->discount}}">
                </div>
            </div>
            <div class="mb-3 row">
                <label for="stock" class="col-sm-2 col-form-label">stock</label>
                <div class="col-sm-4">
                    <input type="number" class="form-control" id="stock"
                           name="stock" placeholder="Stock" value="{{$product[0]->stock}}">
                </div>

                <label for="brand" class="col-sm-2 col-form-label">brand</label>
                <div class="col-sm-4">
                    <input type="text" class="form-control" id="brand"
                           name="brand" placeholder="Brand" value="{{$product[0]->brand}}">
                </div>
            </div>
            <div class="mb-3 row">
                <label for="category" class="col-sm-2 col-form-label">category</label>
                <div class="col-sm-4">
                    <select class="form-select" aria-label="Default select example" name="category">
                        <option {{$product[0]->category}}>{{$product[0]->category}}</option>
                        @foreach($categories as $category)
                            @if($category->category != $product[0]->category)
                            <option value="{{$category->category}}">{{$category->category}}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
                <div class="images-product row">
                    @foreach($images as $image)
                        <div class="col-sm-3">
                            <img class="col-sm-12 float-sm-start" src="{{asset('images/products/'.$image->image)}}">
                            <a href="{{url('delete/'.$image->id)}}" class="btn deleteBtn">X</a>
                            <p style="color: {{$image->color}}">{{$image->color}}</p>

                                <input class="form-check-input" type="radio"
                                       id="main" value="{{$image->main}}"
                                    {{$image->main == 1? ('checked'):('') }}>
                                <label class="form-check-label" for="main">
                                    {{$image->main == 1?'Main':'Not Main' }}
                                </label>
                        </div>
                    @endforeach
                </div>
            </div>

            <button class="btn btn-primary" type="submit">Save</button>
        </form>
    </div>
@endsection
