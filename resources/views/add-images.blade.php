@extends('base')
@section('contant')

{{--    {{dd(Request::path())}}--}}
{{--    {{dd(Request::Route('id'))}}--}}
<div class="add-images">
    <h3>add images</h3>
    <div id="new-image" >
    <form action="{{url('add-images/'.Request::Route('id'))}}" method="post" enctype="multipart/form-data">
        @csrf

        <div class="mb-3 row">
            <label for="image" class="col-sm-2 col-form-label">Images</label>
            <div class="col-sm-10">
                <input id="selectImages" class="form-control form-control-sm"
                       onchange="ShowImages(event)" name="image" type="file" multiple/>
            </div>
        </div>
            <div id="showImages" class="mb-3 row">


            </div>

        <div class="mb-3 row">
            <label for="color" class="col-sm-6 col-form-label">one color for selected images</label>
            <div class="col-sm-6">
                <select class="form-select" aria-label="Default select example"
                        name="color">
                    <option selected>select menu</option>
                    @for($i=0;$i<count($colors);$i++)
                        <option value="{{$colors[$i]}}">{{$colors[$i]}}</option>
                    @endfor
                </select>
            </div>

        </div>
        <div>

        </div>

<button class="btn btn-primary" type="submit">Save</button>
    </form>
    </div>
</div>

@endsection
