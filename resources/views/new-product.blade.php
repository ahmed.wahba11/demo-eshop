@extends('base')
@section('contant')
    <div class="new-product" >
        <h2>Add New Product</h2>
        <form  action="{{route('new-product')}}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="mb-3 row">
                <label for="title" class="col-sm-2 col-form-label">title</label>
                <div class="col-sm-10">
                <input type="text" class="form-control" id="title"
                       name="title" placeholder="Title of the product">
                </div>
            </div>
            <div class="mb-3 row">
                <label for="description" class="col-sm-2 col-form-label">Description </label>
                <div class="col-sm-10">
                <textarea class="form-control" id="description" rows="3" name="description"
                placeholder="Product Description"></textarea>
                </div>
            </div>
            <div class="mb-3 row">
                <label for="price" class="col-sm-2 col-form-label" >price</label>
                <div class="col-sm-4">
                <input type="number" class="form-control" id="price" name="price"
                placeholder="Price">
                </div>

                <label for="discount" class="col-sm-2 col-form-label">discount</label>
                <div class="col-sm-4">
                <input type="number" class="form-control" id="discount"
                       name="discount" placeholder="discount">
                </div>
            </div>
            <div class="mb-3 row">
                <label for="stock" class="col-sm-2 col-form-label">stock</label>
                <div class="col-sm-4">
                <input type="number" class="form-control" id="stock"
                       name="stock" placeholder="Stock">
                </div>

                <label for="brand" class="col-sm-2 col-form-label">brand</label>
                <div class="col-sm-4">
                <input type="text" class="form-control" id="brand"
                       name="brand" placeholder="Brand">
                </div>
            </div>
            <div class="mb-3 row">
                <label for="category" class="col-sm-2 col-form-label">category</label>
                <div class="col-sm-4">
{{--                    <select class="form-select" aria-label="Default select example" name="category">--}}
{{--                        <option selected>select menu</option>--}}
{{--                        @foreach($categories as $category)--}}
{{--                        <option value="{{$category->category}}">{{$category->category}}</option>--}}
{{--                        @endforeach--}}
{{--                    </select>--}}
                    <input type="text" class="form-control" id="category"
                           name="category" placeholder="category">
                </div>

                <label for="owner" class="col-sm-2 col-form-label">owner</label>
                <div class="col-sm-4">
                    <select class="form-select" aria-label="Default select example" name="owner">
                        <option selected>select menu</option>
                        @foreach($users as $user)
                            <option value="{{$user->id}}">{{$user->name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="mb-3 row">
                <label for="image" class="col-sm-2 col-form-label">Photo</label>
                <div class="col-sm-10">
                <input class="form-control form-control-sm" id="image" name="image" type="file">
                </div>
            </div>
            <div class="mb-3 row">
                <label for="color" class="col-sm-2 col-form-label">color</label>
                <div class="col-sm-6">
                    <select class="form-select" aria-label="Default select example"
                            name="color">
                        <option selected>select menu</option>
                        @for($i=0;$i<count($colors);$i++)
                            <option value="{{$colors[$i]}}">{{$colors[$i]}}</option>
                        @endfor
                    </select>
                </div>

            </div>
            <button class="btn btn-primary" type="submit">Save</button>
        </form>
    </div>


@endsection
