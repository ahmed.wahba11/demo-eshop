/**
 * this method for filtering
 * @param className
 * @param id
 */
function filter(className,id) {
    let input,inputValue,main,subMain;
    input = document.getElementById(id);

    inputValue = input.value.toLowerCase();

    main = document.getElementById('main');
    subMain = main.getElementsByClassName(className)

    for(let i=0;i<subMain.length;i++){
        if(subMain[i].innerHTML.toLowerCase().includes(inputValue)){

                subMain[i].parentElement.parentElement.parentElement.style.display = ""

        }else{
                subMain[i].parentElement.parentElement.parentElement.style.display = "none"
        }
    }
}


 function ShowImages(event) {

    if(event.target.files.length > 0){

        for (let i=0 ;i < event.target.files.length;i++){
            let src = URL.createObjectURL(event.target.files[i]);
            let preview = document.getElementById('showImages');
            preview.innerHTML += `<img id="images-selected"  class="img-thumbnail" src="${src}">`

        }
    }if(event.target.files.length === 0) {
        let preview = document.getElementById('showImages');
         preview.innerHTML +=''
    }
}
